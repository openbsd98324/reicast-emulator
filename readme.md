## ROMS

````

ROMS
Accepted File Extensions: .cdi .chd .gdi for Dreamcast ROMs, .zip for Naomi/Atomiswave games.

Place your ROM files in


/home/pi/RetroPie/roms/dreamcast

````

R4 ridge is a .gdi file.

## Stable (Current)

````
https://gitlab.com/openbsd98324/reicast-emulator/-/raw/main/pub/binary/linux/retropie/4.5/armhf/compiled/1647291677-1-reicast-dreamcast-retropie-v2.tar.gz
````


## Testing Reicast

Working: 

````
Siphon
Re-Volt DC
Rayman 2 
````


## DC

````
/home/pi/RetroPie/BIOS/dc
````
